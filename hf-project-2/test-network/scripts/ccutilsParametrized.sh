#!/bin/bash

# installChaincode PEER ORG
function installChaincodeForPeer() {
  ORG=$1
  PEER_PORT=$2
  PEER_NUM=$3
  setGlobalsForPeer $ORG $PEER_PORT
  set -x
  peer lifecycle chaincode install ${CC_NAME}.tar.gz >&log.txt
  res=$?
  { set +x; } 2>/dev/null
  cat log.txt
  verifyResult $res "Chaincode installation on peer${PEER_NUM}.org${ORG} has failed"
  successln "Chaincode is installed on peer${PEER_NUM}.org${ORG}"
}